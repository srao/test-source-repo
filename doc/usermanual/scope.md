---
template: overrides/main.html
title: Scope
---
# Scope of this manual

This section contains both an extensive description of the user interface and configuration possibilities, and a detailed introduction to the code base for potential developers. This manual is designed to:

-   Guide new users through the installation;
-   Introduce new users to the toolkit for the purpose of running their own simulations;
-   Explain the structure of the core framework and the components it provides to the simulation modules;
-   Provide detailed information about all modules and how to use and configure them;
-   Describe the required steps for adding new detector models and implementing new simulation modules.

Within the scope of this document, only an overview of the framework can be provided and more detailed information on the code itself can be found in the Doxygen reference manual @ap2-doxygen available online. No programming experience is required from novice users, but knowledge of (modern) will be useful in the later chapters and might contribute to the overall understanding of the mechanisms.

## Support and Reporting Issues

As for most of the software used within the high-energy particle physics community, only limited support on best-effort basis for this software can be offered. The authors are, however, happy to receive feedback on potential improvements or problems arising. Reports on issues, questions concerning the software as well as the documentation and suggestions for improvements are very much appreciated. These should preferably be brought up on the issues tracker of the project which can be found in the repository @ap2-issue-tracker.

## Contributing Code

Allpix² is a community project that benefits from active participation in the development and code contributions from users. Users and prospective developers are encouraged to discuss their needs either via the issue tracker of the repository @ap2-issue-tracker, the forum @ap2-forum or the developer’s mailing list to receive ideas and guidance on how to implement a specific feature. Getting in touch with other developers early in the development cycle avoids spending time on features which already exist or are currently under development by other users.  The repository contains a few tools to facilitate contributions and to ensure code quality as detailed in Chapter [ch:testing].

* [Getting Started](/getting-started/): Get started with $project
* [Examples](/examples/): Check out some example code!